from django.db import models
from django.conf import settings
# settings.configure()
from django.contrib.auth.models import User
# from utilities import now_plus_days
from constants import CONTENT_CHOICES, TEXT
from datetime import datetime, timedelta
import os


def now_plus_days(no_days=3):
	""" Returns a timestamp object which is no_days after today """
	return datetime.utcnow() + timedelta(days=no_days)


class Display(models.Model):
    name = models.CharField(max_length=100)
    details = models.TextField(max_length=500)

    def __str__(self):
        return self.name


class Channel(models.Model):
    name = models.CharField(max_length=100)
    details = models.TextField(max_length=500)
    displays = models.ManyToManyField(Display)

    def __str__(self):
        return self.name


class Permission(models.Model):
    APPROVER = 'AP'
    ADMIN = 'AD'
    UPLOADER = 'UP'
    PERMISSION_CHOICES = (
        (ADMIN, 'Can extend permissions'),
        (APPROVER, 'Can Approve content'),
        (UPLOADER, 'Can upload content'),
    )
    user = models.ForeignKey(User, db_index=True, related_name='user_with_perm')
    channel = models.ForeignKey(Channel, db_index=True, related_name='permissions')
    permissions = models.CharField(max_length=2, choices=PERMISSION_CHOICES, default=UPLOADER)

    def __str__(self):
        return str(self.user.username) + ' | ' + self.channel.name + ' | ' + self.permissions

    class Meta:
        unique_together = ('user', 'channel')


class Content(models.Model):
    """
        This is the global content upload system.
        One can upload infinite no of items here...

        Some Fields reference:
            - content_type: see constants.py for details
            - timestamp: The timestamp at which it was updated
            - In case of update, the is_approved flag is killed

    """    
    content_type = models.CharField(max_length=5, choices=CONTENT_CHOICES, default=TEXT)
    title = models.CharField(max_length=50)
    description = models.TextField(max_length=200, blank= True)
    link = models.CharField(max_length=500, blank=True)
    user_file = models.FileField(upload_to='content/%Y/%m/', blank=True)
    valid_from = models.DateTimeField('From Date', default=datetime.utcnow)
    valid_till = models.DateTimeField('Valid Till', default=now_plus_days)
    timestamp = models.DateTimeField(auto_now=True)
    submitter = models.ForeignKey(User, related_name='content_submitter')
    channel = models.ForeignKey(Channel)
    approver = models.ForeignKey(User, related_name='content_approver', null=True, blank=True)
    is_approved = models.BooleanField(default=False)
    approve_timestamp = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return '%s, Type=%s' %(self.title, self.content_type)
    
    def get_file_link(self):
        return self.user_file.url
    # def value_to_string(self, obj):
    #     value = self.value_from_object(obj)
    #     return self.get_prep_value(value)


class Quotes(models.Model):
	"""
		id : unique hexadecimal key for each quote
		author : name of the author, though data replication is problem here but
		 it can be solved in future when pifi will become a big company
		text : text of the quote
		quotelink : permanent link of the quote which provides quote in html format
	"""
	id = models.CharField(max_length=10, primary_key=True)
	author = models.CharField(max_length=50)
	text = models.CharField(max_length=1000)
	quotelink = models.CharField(max_length=300)


class Weather(models.Model):
	"""
		temperature : current temperature
		text : text of the weather condition
		quotelink : special code from list of codes yahoo weather api has, this code describes current weather condition and can be used to retrieve
		corresponding image icon
	"""
	temperature = models.IntegerField()
	text = models.CharField(max_length=100)
	code = models.IntegerField()
	timestamp = models.DateTimeField(auto_now=True)


class News(models.Model):
	id = models.IntegerField(primary_key=True)
	zuppit_creation_timestamp = models.DateTimeField()
	source_name = models.CharField(max_length=59)
	title = models.CharField(max_length=300)
	original_url = models.CharField(max_length=300)
	image_link = models.CharField(max_length=300)
	preview = models.CharField(max_length=1000)
	added_timestamp = models.DateTimeField(auto_now=True)


class Notice(models.Model):
    """
        timestamp: the upload timestamp.
        metadata: Some extra metadata which can be specified by the user.

        We need some support for "channels"
         - A user can post a notice in some channel(s)
         - Every notice board would subscribe to some channel
    """
    user = models.ForeignKey(User)
    # category = models.CharField(max_length=6, choices=NOTICE_CATEGORIES, default='SIM')
    title = models.CharField(max_length=50, blank=True)
    content = models.TextField(max_length=500, blank=True)
    image_link = models.CharField(max_length=300, blank=True)
    image = models.ImageField(upload_to='images/%Y/%m/', blank=True)
    from_timestamp = models.DateTimeField('From Date', default=datetime.utcnow)
    to_timestamp = models.DateTimeField('Valid Till', default=now_plus_days)
    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '%s %s %s'%(self.title, str(self.timestamp), self.content)
