# db_help.py - contains helper functions for accessing the database.
from django.conf import settings
from models import Notice, Quotes, Weather, News
from constants import *
from datetime import datetime, timedelta
import math
import json 
import requests
# ------- models helper ---------




# ------- database ---------

def format_TS(dt):
	return dt.strftime('%Y-%m-%d %H:%M:%S')

def __change_TS(dct):
	"""Because Timestamp is not json serializable, hence we change it in string
		format
	"""
	for k, v in dct.iteritems():
		if type(v) == datetime:
			dct[k] = format_TS(v)
	return dct

def get_latest_notices():
	now = datetime.now()
	rows = Notice.objects.filter(from_timestamp__lt=now, to_timestamp__gt=now) \
		.order_by('-timestamp')[0:MAX_NOTICES]
	ans = [__change_TS(row) for row in rows.values()]
	for notice in ans:
		if ( bool(notice['image']) ):
			print str(notice)
			notice['image_link'] = settings.MEDIA_URL[1:] + notice['image']
		del notice['image']
	ans.reverse()
	return ans


def quote_miner():
	response = requests.get(QUOTE_URL, proxies=PROXIES)
	if(response.status_code==200):
		response_data = response.json()
		quote_id = response_data['quoteLink'].split('/')[-2]
		new_quote = Quotes.objects.get_or_create(id=quote_id,\
			author = response_data['quoteAuthor'],\
			text = response_data['quoteText'],\
			quotelink = response_data['quoteLink'])


def weather_miner():
	response = requests.get(WEATHER_URL, proxies=PROXIES)
	if(response.status_code==200):
		response_data = response.json()
		condition_dict = response_data['query']['results']['channel']['item']['condition']
		temp = condition_dict['temp']
		condition = condition_dict['text']
		new_weather_item = Weather.objects.create(temperature=int(temp),\
			text = condition,\
			code = int(condition_dict['code']))

def news_miner():
	headers = {'Authorization': 'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnX2lhdCI6MTQ1NDk2NDAzMCwidXNlcl9pZCI6ODc2LCJleHAiOjE0ODY1MDAwODQsInVzZXJuYW1lIjoicGlmaXNtYXJ0Ym9hcmRzIiwiZW1haWwiOiIifQ.8uowq4v0RYBcTLtKBO_2mIeQ60EqGoB8vYlpEzxoRnA'}
	response = requests.get(NEWS_URL, headers=headers, proxies=PROXIES)
	if(response.status_code==200):
		response_data = response.json()
		newsList = response_data['data']
		query_time = datetime.now()
		for news in newsList:
			try:
				news_obj = News.objects.create(id = news['id'],\
					zuppit_creation_timestamp = datetime.fromtimestamp(float(news['creation_timestamp'])),\
					source_name = news['source_name'],\
					title = news['title'],\
					original_url = news['url'],\
					image_link = news['image_link'],\
					preview = news['preview'],\
					added_timestamp = query_time) 
			except:
				print 'Could not add news for some reason'

# Which form??
def validate_form_data(data, files):
	try:
		content_type = data['content_type']
		title = data['title']
		description = data['description']
		link = data['link']
		channel_id = data['channel_id']
		print files
	except:
		return 'Bad data received' 

	if (len(title) == 0):
		return 'Title Missing'
	if (channel_id < 0):
		return 'Channel To Post Missing'
	try:
		valid_from = datetime.strptime(data['valid_from'][0:19], '%Y-%m-%dT%H:%M:%S')
	except:
		return 'Bad input for \'valid from\' date'
	try:
		valid_till = datetime.strptime(data['valid_till'][0:19], '%Y-%m-%dT%H:%M:%S')
	except:
		return 'Bad input for \'valid till\' date'

	if (content_type == 0):
		if (len(description) < 10):
			return 'Too Short Description'
	if content_type == 1:
		if (link == '' and len(files)==0):
			return 'No file or link to get Image.';
		if (not (link.startswith('http://') or link.startswith('https://')) and len(files)==0):
			return 'Given link is not a valid URL';
	if content_type == 2:
		if (link == '' and len(files)==0):
			return 'No file or link to get PDF.';
		if (not (link.startswith('http://') or link.startswith('https://')) and len(files)==0):
			return 'Given link is not a valid URL.';
	if content_type == 3:
		if (not (link.startswith('http://') or link.startswith('https://'))):
			return 'Given link should begin with https://docs.google.com/presentation';

	return VALID_FORM;


if __name__ == '__main__':	
	print(get_random_quote())
