
from datetime import datetime
import json, decimal
import requests as url_requests
from bs4 import BeautifulSoup
import json
from django.shortcuts import render, render_to_response
from django.http import HttpResponse as HR, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.views.generic import View
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.response import Response
from rest_framework.views import APIView
from django.core import serializers
from constants import *
from .models import *
from .utilities import *


# Create your views here.
def __datetime_to_timestamp(dt):
    """Converts a datetime object to UTC timestamp"""
    return int((dt.replace(tzinfo=None) - datetime(1970,1,1)).total_seconds())

def __getItem(request, key, default=None):
    if (USING_GET):
        return request.GET.get(key, default)
    return request.POST.get(key, default)

# the default becomes none in this case
def __errorReponse(info):
    return HR(json.dumps({'status': 0, 'info': info, 'data': {}}), \
        content_type="application/json")

def __HR(data, info=''):
    return HR(json.dumps({'status': 1, 'info': info, 'data': data}), \
        content_type="application/json")

# Views begin from here ----------------
def ff(file):
    if (file):
        return file.url
    return ''

def get_notices(request):
    """Gives 20 latest notices, which came after the provided timestamp"""

    display_id = __getItem(request, 'id')
    if display_id is None:
        return __errorReponse('Expected Display-ID along with request')

    curr_time = datetime.now()
    notices = Content.objects.select_related().filter( \
        channel__displays__id=display_id, \
        is_approved=True, \
        valid_from__lte=curr_time, \
        valid_till__gte=curr_time \
        ).order_by('-timestamp')[0:MAX_NOTICES]
    published_content =[]
    for notice in notices:
        ans = {
            'id' : notice.id, \
            'type' : notice.content_type, \
            'title' : notice.title, \
            'description' : notice.description, \
        }
        if (notice.user_file):
            ans['url'] = "http://notices.smartcampus.iitd.ernet.in" + notice.user_file.url
            # ans['url'] = "http://localhost" + notice.user_file.url
        elif len(notice.link):
            ans['url'] = notice.link
        published_content.append(ans)
    return __HR(published_content)


def get_channels(request):
    channels = Channel.objects.all()
    published_content =[]
    for channel in channels:
        ans = {
            'id' : channel.id, \
            'name' : channel.name, \
            'description' : channel.details, \
        }
        published_content.append(ans)
    return __HR(published_content)

def get_channel_notices_phone(request):
    id = __getItem(request, 'id')
    if(Channel.objects.filter(id=id).exists()):
        channel = Channel.objects.get(id = id)
    else:
        return Response({'msg':'Channel-ID Item Does exist in our database, please supply digestable channel_id', 'data' : [], 'error' :1})
    curr_time = datetime.now()
    notices = Content.objects.filter( \
        channel=channel, \
        is_approved=True, \
        valid_from__lte=curr_time, \
        valid_till__gte=curr_time \
        ).order_by('-timestamp')[0:MAX_NOTICES]
    published_content =[]
    for notice in notices:
        ans = {
            'id' : notice.id, \
            'type' : notice.content_type, \
            'title' : notice.title, \
            'description' : notice.description, \
            'channel' : notice.channel.name, \
        }
        if (notice.user_file):
            ans['url'] = "http://notices.smartcampus.iitd.ernet.in" + notice.user_file.url
            # ans['url'] = "http://localhost" + notice.user_file.url
        elif len(notice.link):
            ans['url'] = notice.link
        published_content.append(ans)
    return __HR(published_content)


class get_channel_notices(APIView):
    """
    Get Channel Notices
    """
    def get(self, request, *args, **kwargs):
        params = request.query_params
        if 'channel_id' not in params:
            return __errorReponse('Channel-ID Missing, we cannot supply notices if donot give us channel_id')
        channel_id = int(params['channel_id'])
        if(Channel.objects.filter(id=channel_id).exists()):
            channel = Channel.objects.get(id = channel_id)
        else:
            return Response({'msg':'Channel-ID Item Does exist in our database, please supply digestable channel_id', 'data' : [], 'error' :1})
        channel_published_content_data = Published_content.objects.filter(channel=channel)
        data = [{'id' : published_content_item.content.id, 'type' : published_content_item.content.content_type, 'title' : published_content_item.content.title, 'description':published_content_item.content.description, 'approved' : published_content_item.is_approved, 'publisher' : published_content_item.content.submitter.username} for published_content_item in channel_published_content_data]
        return Response({'msg':'You Retrieved Channel Content Successfully', 'channel_data' : data, 'error' :0})

# -------------a special case
def index(request):
    context = RequestContext(request)
    pi_id = __getItem(request, 'id')
    if pi_id is None:
        pi_id='1'
    context_dict = {'id': pi_id}
    return render_to_response('html/index.html', context_dict, context)

def testing_template(request):
    context = RequestContext(request)
    context['id'] = __getItem(request, 'id', default=1)
    return render_to_response('testing.html', context)

def view_login(request, from_login=None):
    context = {}
    if request.method == 'POST' and request.POST:
        logout(request)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/my_channels/')
            else:
                context['errors'] = 'Invalid Password'
        else:
            context['errors'] = 'Invalid Password'
    return render(request, 'main_site/login.html', context)

def view_logout(request):
    logout(request)
    return render(request, 'main_site/logout.html', {})


# @method_decorator(login_required) - decorator status unknown
def view_my_contents(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')

    context = {'error': 0}
    user = request.user
    my_content = Content.objects.filter(submitter = user)
    # channel_id = int(channel_id)
    data = [
        {
            'id' : content.id, 
            'type' : content.content_type, 
            'title' : content.title, 
            'description':content.description, 
            'approved' : content.is_approved, 
            'link' : content.link, 
            'timestamp' : content.timestamp, 
            'channel' : content.channel, 
            'approver':content.approver, 
            'approve_timestamp' : content.approve_timestamp, 
        } 
        for content in my_content 
    ]
    context['my_content'] = data
    # context['channel_id'] = channel_id
    return render(request, 'main_site/my_contents.html', context)


def view_my_content(request, content_id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')

    content = Content.objects.get(id=int(content_id))
    # TODO: CHECK PERMISSION HERE...
    return render(request, 'main_site/content_detail.html', locals())


def pi_cards(request, card_type):
    context = RequestContext(request)
    return render_to_response('cards/'+card_type+'.html', context)


def view_channel_content_page(request, channel_id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')

    try:
        channel_id = int(channel_id)
        channel = Channel.objects.get(id = channel_id)
    except:
        return __errorReponse("Channel ID does not exist")

    # There shall be hopefully ONLY one db hit.
    admins = channel.permissions.filter(
        permissions=Permission.ADMIN
    )
    approvers = channel.permissions.filter(
        permissions=Permission.APPROVER
    )
    uploaders = channel.permissions.filter(
        permissions=Permission.UPLOADER
    )
    
    contents = Content.objects.filter(channel=channel).order_by('-timestamp')[
        0:MAX_CHANNEL_NOTICES_DISPLAY
    ]

    # Muaaah - the awesome locals() trick!!!
    return render(request, 'main_site/channel_content.html', locals())


def approve_content(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    content_id = request.POST.get('content_id', '')
    if(content_id!=''):
        content_id = int(content_id)
    try:
        content = Content.objects.get(id = content_id)
    except Content.ObjectDoesNotExist:
        return __errorReponse('Supplied Content Id Does not exist')
    content.is_approved  = True
    content.save()
    return __HR('Content Approve Successfull')



def view_my_channels(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    rows = Permission.objects.filter(
        user=request.user
    ).values('channel_id', 'channel__name', 'channel__details', 'permissions')
    admin_channels = []; approve_channels = []; upload_channels = []
    for row in rows:
        if row['permissions'] == Permission.ADMIN:
            admin_channels.append(row)
        elif row['permissions'] == Permission.APPROVER:
            approve_channels.append(row)
        else:
            upload_channels.append(row)
    context = {}
    context['admin_channels'] = admin_channels
    context['approve_channels'] = approve_channels
    context['upload_channels'] = upload_channels
    return render(request, 'main_site/my_channels.html', context)


def view_add_notice(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login/')
    channels = Channel.objects.all().values('id', 'name')
    context = RequestContext(request, {
        'channels': channels
    })
    print channels
    return render_to_response('add_notice.html', context)


"""
view function to handle submitted notice by any user
"""
def submit_notice(request):
    context = RequestContext(request)
    data = json.loads(request.POST.get('data'))
    is_valid_form = validate_form_data(data, request.FILES);

    if is_valid_form != VALID_FORM:
        return __errorReponse(is_valid_form)

    content_type = data['content_type']
    title = data['title']
    description = data['description']
    link = data['link']
    channel_id = data['channel_id']
    valid_from = datetime.strptime(data['valid_from'][0:19], '%Y-%m-%dT%H:%M:%S')
    valid_till = datetime.strptime(data['valid_till'][0:19], '%Y-%m-%dT%H:%M:%S')
    files = request.FILES
    file = ''
    print 'reach'

    if((content_type == 1 or content_type == 2) and link == ''):
        try: 
            file = files['file']
        except:
            return __errorReponse('No file or link found for Image or PDF')

    if (content_type == 0):
        link = '';
        content_type = TEXT
    elif (content_type == 1):
        if(len(files) != 0):
            link='';
        description = ''
        content_type = IMAGE
    elif (content_type == 2):
        if(len(files) != 0):
            link='';
        description = ''
        content_type = PDF
    elif (content_type == 3):
        description = ''
        content_type = PPT

    try:
        print 'reach'
        uploaded_content = Content(content_type=content_type, title=title, description=description, link=link, user_file=file, valid_from=valid_from, valid_till=valid_till, submitter=request.user, channel_id=channel_id)
        print 'reach'
        uploaded_content.save()
    except Exception as e:
        print '%s (%s)' % (e.message, type(e))
        return __errorReponse('Something went wrong')

    return __HR(data={}, info='Notice added successfully')


"""
get news from local news database
"""
def get_news(request):
    context = RequestContext(request)
    newsList = News.objects.values('title', 'image_link').order_by('-added_timestamp')[0:MAX_NEWS]
    data = [{
            'title': news['title'],
            'image': news['image_link'] 
        } for news in newsList
    ]
    return __HR(data)


"""
api view to get current weather form local db
"""
def get_weather(request):
    context = RequestContext(request)
    weather = Weather.objects.values('temperature', 'text', 'code', 'timestamp').order_by('-timestamp').first()
    data =  {
        'text': weather['text'],
        'temperature': int(weather['temperature']),
        'icon': WEATHER_IMAGE[int(weather['code'])],
        'last_updated': str(weather['timestamp'])
    }
    return __HR(data)

"""
api to get quote from quotation database
"""
def get_quote(request):
    context = RequestContext(request)
    quote = Quotes.objects.values('author', 'text').order_by('?').first()
    data =  {
        'text': quote['text'],
        'author': quote['author'],
        'last_updated': str(datetime.now())
    }
    return __HR(data)


"""
base site which can be extended to make any page on notice board admin site
"""
def base_site(request):
    return render(request, 'main_site/base.html', {})


def get_bookings_data(request):
    """
        Gives the schedule for the asked timestamp, area (id e.g. 1) and room (id e.g. 1)
    """
    try:
        str_timestamp = __getItem(request, 'timestamp')
        timestamp = datetime.strptime(str_timestamp, "%Y-%m-%d")
        area = int(__getItem(request, 'area'))
        room = int(__getItem(request, 'room'))
    except:
        return __errorReponse('Bad input format')
    day = timestamp.day
    month = timestamp.month
    year = timestamp.year
    base_url = 'https://poorvi.cse.iitd.ernet.in/mrbs/week.php?'
    whole_url = ''.join([base_url,'year=', str(year),'&month=', str(month),'&day=', str(day),'&area=', str(area),'&room=', str(room)])
    # print whole_url
    url_response = url_requests.get(whole_url, verify=False) # Because of no certificate for Poorvi
    html_response = url_response.text
    day_date = int(day);
    soup = BeautifulSoup(html_response,'html.parser')
    rows = soup.find_all('tbody')
    schedule_body = rows[3]
    all_rows = schedule_body.find_all('tr')
    i = 0;
    response = [];
    time_mapping = ['07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30']
    for row in all_rows:
        all_columns = row.find_all('td',{'class':"I"})
        for col in all_columns:
            current_day = col
            class_name = ''.join(current_day['class'])
            if(class_name=='I'):
                one_response = {}
                a_element = current_day.find('a')
                a_href = a_element['href']
                splited_href = str.split(a_href.encode('ascii','ignore'),'&')
                splited_day = str.split(splited_href[2],'=')
                if (int(splited_day[1])==day_date):
                    div_element = current_day.find('div')
                    class_name = ''.join(div_element['class'])
                    title = a_element['title']
                    text = a_element.getText()
                    one_response['start_time'] = time_mapping[i]
                    one_response['title'] = title.encode('ascii','ignore')
                    one_response['text'] = text.encode('ascii','ignore')
                    class_time = int(class_name[7:])
                    one_response['duration'] = class_time*0.5

                    # else:
                    #   one_response['duration'] = 100;
                    response.append(one_response)
        i = i+1;
    return __HR(response)