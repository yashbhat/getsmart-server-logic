# constants.py... think of this as the global variables in the project
import urllib

DEBUG = True
USING_GET = True    # if this is toggled then the server looks for data in the post format.
MAX_NOTICES = 20
MAX_NEWS = 20
VALID_FORM = 'valid'

MAX_CHANNEL_NOTICES_DISPLAY = 40

QUOTE_URL = "http://api.forismatic.com/api/1.0/?method=getQuote&format=json&lang=en"

query = urllib.quote_plus('select * from weather.forecast where woeid in (select woeid from geo.places(1) where text="delhi, in") and u="c"')
WEATHER_URL = "https://query.yahooapis.com/v1/public/yql?q=" +  query + "&format=json"

NEWS_URL = "http://zuppit.in/smartcampus/getlatestnews"

TEXT = 'text'
PPT = 'ppt'
IMAGE = 'img'
PDF = 'pdf'
CONTENT_CHOICES = (
    (TEXT, 'textual content'),
    (PPT, 'google slide link'),
    (IMAGE, 'user uploaded jpg/gif/png'),
    (PDF, 'user uploaded PDF'),
)

PROXIES = {
  "http": "http://10.10.78.62:3128",
  "https": "http://10.10.78.62:3128",
}

WEATHER_IMAGE = {
    0: 'wi-tornado',
    1: 'wi-storm-showers',
    2: 'wi-tornado',
    3: 'wi-thunderstorm',
    4: 'wi-thunderstorm',
    5: 'wi-snow',
    6: 'wi-rain-mix',
    7: 'wi-rain-mix',
    8: 'wi-sprinkle',
    9: 'wi-sprinkle',
    10: 'wi-hail',
    11: 'wi-showers',
    12: 'wi-showers',
    13: 'wi-snow',
    14: 'wi-storm-showers',
    15: 'wi-snow',
    16: 'wi-snow',
    17: 'wi-hail',
    18: 'wi-hail',
    19: 'wi-cloudy-gusts',
    20: 'wi-fog',
    21: 'wi-fog',
    22: 'wi-fog',
    23: 'wi-cloudy-gusts',
    24: 'wi-cloudy-windy',
    25: 'wi-thermometer',
    26: 'wi-cloudy',
    27: 'wi-night-cloudy',
    28: 'wi-day-cloudy',
    29: 'wi-night-cloudy',
    30: 'wi-day-cloudy',
    31: 'wi-night-clear',
    32: 'wi-day-sunny',
    33: 'wi-night-clear',
    34: 'wi-day-sunny-overcast',
    35: 'wi-hail',
    36: 'wi-day-sunny',
    37: 'wi-thunderstorm',
    38: 'wi-thunderstorm',
    39: 'wi-thunderstorm',
    40: 'wi-storm-showers',
    41: 'wi-snow',
    42: 'wi-snow',
    43: 'wi-snow',
    44: 'wi-cloudy',
    45: 'wi-lightning',
    46: 'wi-snow',
    47: 'wi-thunderstorm',
    3200: 'wi-cloud'
}