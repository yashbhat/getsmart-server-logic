from django.contrib import admin

# Register your models here.
from .models import Notice, Quotes, Weather, Display, Channel, \
	Permission, Content, News

class NoticeAdmin(admin.ModelAdmin):
	fieldsets = [
		(None, {'fields': ['user']}),
		('CONTENT', {
			'fields': ['title', 'content', 'image_link', 'image'],
			'description': "Hint: image_link can be from any source on the " +
				"Internet, OPTIONALLY you can upload a file from your system",
		}),
		('VALIDITY', {'fields': ['from_timestamp', 'to_timestamp']} ),
	]
	readonly_fields=('user',)
	def has_add_permission(self, request):
		return (request.user is not None)

	def has_change_permission(self, request, obj=None):
		if (obj == None):
			return (request.user is not None)
		return (obj.user == request.user or request.user.is_superuser)

	def has_delete_permission(self, request, obj=None):
		if (obj == None):
			return (request.user is not None)
		return (obj.user == request.user)

	def save_model(self, request, obj, form, change):
		obj.user = request.user
		super(NoticeAdmin, self).save_model(request, obj, form, change)

	def get_form(self, request, obj=None, **kwargs):
		self.exclude = ("user", "timestamp")
		form = super(NoticeAdmin, self).get_form(request, obj, **kwargs)
		return form

	def get_queryset(self, request):
		qs = super(NoticeAdmin, self).get_queryset(request)
		print 'evaluating queryset'
		if request.user.is_superuser:
			return qs
		return qs.filter(user=request.user)

class QuotesAdmin(admin.ModelAdmin):

	model = Quotes
	list_display = ['id', 'text', 'author', 'quotelink']


class WeatherAdmin(admin.ModelAdmin):

	model = Weather
	list_display = ['id', 'text', 'temperature', 'code']
		

admin.site.register(Notice, NoticeAdmin)
admin.site.register(Quotes, QuotesAdmin)
admin.site.register(Weather, WeatherAdmin)
admin.site.register(Display)
admin.site.register(Channel)
admin.site.register(Permission)
admin.site.register(Content)
admin.site.register(News)