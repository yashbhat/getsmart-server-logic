"""smartX URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from bus_tracker import urls as notice_urls
from bus_tracker.views import *
from django.conf.urls.static import static
from smartX import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.generic import TemplateView


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^notices/', include(notice_urls)),
    url(r'^approveContent/$', approve_content),
    url(r'^cards/(?P<card_type>[a-zA-Z0-9_]+)/$', pi_cards),

    # main site
    url(r'^$', index),
    url(r'^testing/$', testing_template),
    url(r'^login/$', view_login, name='login'),
    url(r'^logout/$', view_logout, name='logout'),
    
    url(r'^my_channels/$', view_my_channels, name='my_channels'),
    url(r'^my_channels/(?P<channel_id>\d+)/$', 
        view_channel_content_page , 
        name='my_channel'
    ),
    
    url(r'^my_contents/$', view_my_contents, name='my_contents'),
    url(r'^my_content/(?P<content_id>\d+)/$', view_my_content, name='my_content'),

    url(r'^addNotice/$', view_add_notice),
    
    # Testing here
    url(r'^base/$', base_site),
    url(r'^tablestesting/$', view_channel_content_page),
] 
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
