var app = angular.module('smart-display-admin', []);


// initialize input widgets
// ptTimeSelect doesn't trigger change event by default
$('#validity .time').ptTimeSelect({
    'onClose': function($self) {
        $self.trigger('change');
    }
});

$('#validity .date').pikaday();

var TIMEFORMAT = 'h:mm a';
var datepair = new Datepair(document.getElementById('validity'), {
    parseTime: function(input){
        // use moment.js to parse time
        var m = moment(input.value, TIMEFORMAT);
        return m.toDate();
    },
    updateTime: function(input, dateObj){
        var m = moment(dateObj);
        return m.format(TIMEFORMAT);
    },
    parseDate: function(input){
        var picker = $(input).data('pikaday');
        return picker.getDate();
    },
    updateDate: function(input, dateObj){
        var picker = $(input).data('pikaday');
        return picker.setDate(dateObj);
    }
});

function approve(elem) {
    var contetnId = $(elem).parent().parent().attr('data-notice');
    var csrftoken = getCookie('csrftoken');

    $.ajax({
        url: '/approveContent/',
        type: 'post',
        headers: {
            'Content-Type': undefined,
            'X-CSRFToken': csrftoken
        },
        data: {
            'content_id': contetnId
        },
        dataType: 'json',
        success: function (response) {
            console.log(response);
            if(response.status == 0) {
                alert(response.info);
                return
            }
            elem.innerHTML = 'Approved';
            $(elem).addClass('btn-success disabled');
            $(elem).removeClass('btn-warning');
            // console.log(response);
        },
        error: function(xhr, textStatus, errorThrown){
            alert('OOPS!!! Something went wrong.');
        }

    });
    return; 
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

app.controller('MainController', ['$scope', '$http', function($scope, $http) {

    var vm = this;
    VIRGIN_NOTICE_2 = {
        content_type: 0,
        title: 'hahaha woooh',
        description: 'Mux is awesome',
        link: 'http://leleo.com',
        channel_id: -1,
        valid_from: new Date(2010, 11, 28, 14, 57),
        valid_till: new Date(2010, 11, 28, 14, 57),
    };
    VIRGIN_NOTICE = {
        content_type: 0,
        title: '',
        description: '',
        link: '',
        channel_id: -1,
        valid_from: new Date(2010, 11, 28, 14, 57),
        valid_till: new Date(2010, 11, 28, 14, 57),
    };
    DEFAULT_UPLOAD_ERROR = 'Something went wrong.';
    vm.notice = VIRGIN_NOTICE;
    vm.upload_error = '';
    vm.channelNotices = [];

    vm.content_type_select = function(event) {
        vm.notice.content_type = parseInt(event.target.value);
    }

    vm.is_valid_form = function() {
        if(vm.notice.content_type > 3 || vm.notice.content_type < 0) {
            vm.upload_error = 'Select Notice Content Type.';
            return false;
        }
        if(vm.notice.title.length == 0){
            vm.upload_error = 'Notice Title Missing.';
            return false;
        }
        if(vm.notice.channel_id < 0) {
            vm.upload_error = 'Channels to Post Notice Missing.';
            return false;
        }
        if(isNaN(vm.notice.valid_from.getTime())) {
            vm.upload_error = '\'Valid From\' date Incorrect.';
            return false;
        }
        if(isNaN(vm.notice.valid_till.getTime())) {
            vm.upload_error = '\'Valid Till\' date Incorrect.';
            return false;
        }
        if($('#valid-from-time').val()=="") {
            vm.upload_error = '\'Valid From\' time Incorrect.';
            return false;
        }
        if($('#valid-to-time').val()=="") {
            vm.upload_error = '\'Valid Till\' time Incorrect.';
            return false;
        }


        vm.notice.valid_from = new Date($('#valid-from-date').val() + " " + $('#valid-from-time').val());
        vm.notice.valid_till = new Date($('#valid-to-date').val() + " " + $('#valid-to-time').val());

        if(vm.notice.valid_from >= vm.notice.valid_till) {
            vm.upload_error = '\'Valid From\' date-time should be less than \'Valid Till\' date-time.';
            return false;
        }

        var is_file_attached = ($('#input-notice-file')[0].files.length>0);

        switch(vm.notice.content_type) {
            case 0:
                if(vm.notice.description.length < 10) {
                    vm.upload_error = 'Description should be atleast 10 characters long.';
                    return false;
                }
                vm.notice.link = '';
                break;
            case 1:
                if(vm.notice.link == '' && !is_file_attached) {
                    vm.upload_error = 'Either Attach File or Insert link.';
                    return false;
                }
                else if(!vm.notice.link.startsWith('http://') && !vm.notice.link.startsWith('https://') && !is_file_attached) {
                    vm.upload_error = 'Invalid Link URL.';
                    return false;
                }
                if(is_file_attached)
                    vm.notice.link = '';
                vm.notice.description = '';
                break;
            case 2:
                if(vm.notice.link == '' && !is_file_attached) {
                    vm.upload_error = 'Either Attach File or Insert link.';
                    return false;
                }
                else if(!vm.notice.link.startsWith('http://') && !vm.notice.link.startsWith('https://') && !is_file_attached) {
                    vm.upload_error = 'Invalid Link URL.';
                    return false;
                }
                if(is_file_attached)
                    vm.notice.link = '';
                vm.notice.description = '';
                break;
            case 3:
                if(!vm.notice.link.startsWith('https://docs.google.com/presentation')) {
                    vm.upload_error = 'Invalid Link URL. Only google docs URLs are accepted for uploading PPT.';
                    return false;
                }
                vm.notice.description = '';
                break;
        }
        return true;
    }

    vm.addNotice = function() {

        vm.notice.valid_from = new Date($('#valid-from-date').val() + " " + $('#valid-from-time').val());
        vm.notice.valid_till = new Date($('#valid-to-date').val() + " " + $('#valid-to-time').val());

        if(!vm.is_valid_form()) {
            console.log(vm.upload_error)
            $('#submit-notice-modal').modal({
                show: true,
            });
            return 'Found errors!!!';
        }

        var fd = new FormData();
        fd.append('data', angular.toJson(vm.notice));

        if ((vm.notice.content_type == 1 || vm.notice.content_type == 2) && $('#input-notice-file')[0].files.length>0)
            fd.append('file', $('#input-notice-file')[0].files[0]);

        var csrftoken = getCookie('csrftoken');

        $http.post('/notices/submitNotice/', fd, {
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                'X-CSRFToken': csrftoken
            }
        }).success(function (response) {
            // console.log(response);
            if(response.status == 0) {
                vm.upload_error = response.info;
            }
            else {
            vm.upload_error = '';
                vm.notice = VIRGIN_NOTICE;
            }
            $('#submit-notice-modal').modal({
                show: true,
            });
            return;
            // console.log(response);
        }).error(function (response) {
            vm.upload_error = DEFAULT_UPLOAD_ERROR;
            $('#submit-notice-modal').modal({
                show: true,
            });
            // console.log(response);
        });
    }

    // vm.getChannelNotices = function() {
    getChannelNotices = function() {
        $http.get ('/notices/getChannelNotices/?channel_id=1').success(function (response) {
            if(response.error != 0) 
                return;
            
            vm.channelNotices = response.channel_data;
            console.log(vm.channelNotices);
        }).error(function (response) {
            console.log(response);
        });
    }

    // getChannelNotices()

    vm.showResponse = function() {
        
    }

}]);