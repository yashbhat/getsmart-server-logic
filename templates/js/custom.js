var app = angular.module('smart-display-app', []);
var MAX_NOTIFICATIONS = 4;
var HOST = location.hostname+':'+location.port;
var TOKEN = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmlnX2lhdCI6MTQ1NDk2NDAzMCwidXNlcl9pZCI6ODc2LCJleHAiOjE0ODY1MDAwODQsInVzZXJuYW1lIjoicGlmaXNtYXJ0Ym9hcmRzIiwiZW1haWwiOiIifQ.8uowq4v0RYBcTLtKBO_2mIeQ60EqGoB8vYlpEzxoRnA';

app.controller('MainController', ['$scope', '$http', '$interval', function($scope, $http, $interval) {
    var vm = this;
    vm.notices = [];
    vm.newsList = [{
        title: 'Loading...',
        image: '/static/images/loading.gif'
    }];
    vm.weather = {};
    vm.clock = Date.now();
    vm.timeTable=[];
    var newsIndexFocus = 0;
    var noticeIndexFocus = 0;
    var lastUpdated = '2010-12-15 01:21:05';


    function getNotices() {
        $http({
            method: 'GET',
            url: 'http://' + HOST + '/notices/getNotices/',
            headers: {
                'Content-Type': "application/json"
            },
            params: {
                'id': ID
            }
        }).then(function successCallback(response) {
            // console.log(response.data);
            if (response.data.data == null || response.data.data.length == 0 || response.data.status != 1)
                return;
            console.log('Notice Fetch - Success :)');
            vm.notices = response.data.data;
        }, function errorCallback(response) {
            console.log('Notices - Some Error Occurred :(');
        });

    }
    getNotices();
    $interval(getNotices, 1800 * 1000); // 30 minutes

    function updateNews() {
        $http({
            method: 'GET',
            url: 'http://' + HOST + '/notices/getNews/',
            headers: {
                'Content-Type': "application/json"
            },
        }).then(function successCallback(response) {
            // console.log(response);
            if(response.data.status != 1)
                return;
            console.log('News fetched successfullly');
            vm.newsList = response.data.data;
            // console.log(vm.newsList);
        }, function errorCallback(response) {
            console.log('News error detected');
            console.log(response);
        });

    }
    updateNews();
    $interval(updateNews, 1800 * 1000); // 30 minutes

    function updateWeather() {
        $http({
            method: 'GET',
            url: 'http://' + HOST + '/notices/getWeather/',
            headers: {
                'Content-Type': "application/json"
            },
        }).then(function successCallback(response) {
            // console.log(response);
            if(response.data.status != 1)
                return;
            console.log('Weather fetched successfullly');
            vm.weather = response.data.data;
        }, function errorCallback(response) {
            console.log('Weather error detected');
            // console.log(response);
        });

    }
    updateWeather();
    $interval(updateWeather, 1800 * 1000); // 30 minutes

    function updateQuote() {
        $http({
            method: 'GET',
            url: 'http://' + HOST + '/notices/getQuote/',
            headers: {
                'Content-Type': "application/json"
            },
        }).then(function successCallback(response) {
            // console.log(response);
            if(response.data.status != 1)
                return;
            console.log('Quote fetched successfullly');
            vm.quote = response.data.data;
        }, function errorCallback(response) {
            console.log('Quote error detected');
            // console.log(response);
        });

    }
    updateQuote();
    $interval(updateQuote, 1800 * 1000); // 30 minutes

    function clockTick($scope, $timeout) {
        vm.clock = Date.now();
    }
    $interval(clockTick, 1*1000); // 10 seconds

    function newsSlide() {
        $('.right-drawer').animate({
            scrollTop: $($(".right-drawer").children().get(newsIndexFocus)).offset().top - $($(".right-drawer").children().get(0)).offset().top
        }, 500);
        newsIndexFocus = (newsIndexFocus + 1) % (vm.newsList.length);
    }
    $interval(newsSlide, 6*1000); // 6 seconds
    
    function noticeSlide() {
        $('.main-content').animate({
            scrollTop: $($(".main-content").children().get(noticeIndexFocus)).offset().top - $($(".main-content").children().get(0)).offset().top
        }, 500);
        noticeIndexFocus = (noticeIndexFocus + 1) % (vm.notices.length);
    }
    $interval(noticeSlide, 10*1000); // 8 seconds

}]);

app.directive('piCard', function($interpolate) {
    return {
        scope: {
            data: '=info'
        },
        templateUrl: function(elem, attr) {
            return 'http://'+ HOST +'/cards/'+attr.type+'_card/';
        }
    };
});